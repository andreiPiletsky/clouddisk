package netcomteam.com.tgclouddisk.user;
import android.util.Log;

import org.drinkless.td.libcore.telegram.TdApi;
/**
 * Created by andreika on 26.09.17.
 */

public class UserInformation {
    private static int id;
    private static String firstName;
    private static String lastName;
    private static String username;
    private static String phone_number;
    private static TdApi.UserStatus status;

    public UserInformation() {

    }

    public static int getId() {
        return id;
    }

    public static String getFirstName() {
        return firstName;
    }

    public static String getLastName() {
        return lastName;
    }

    public static String getUsername() {
        return username;
    }



    public static TdApi.UserStatus getStatus() {
        return status;
    }


    public static String getPhone_number() {
        return phone_number;
    }

    public static void setPhone_number(String phone_number) {
        UserInformation.phone_number = phone_number;
    }

    public static void setId(int id) {
        UserInformation.id = id;
        Log.w("idddddddddddddd", String.valueOf(id));
    }

    public static void setFirstName(String firstName) {
        UserInformation.firstName = firstName;
    }

    public static void setLastName(String lastName) {
        UserInformation.lastName = lastName;
    }

    public static void setUsername(String username) {
        UserInformation.username = username;
    }



    public static void setStatus(TdApi.UserStatus status) {
        UserInformation.status = status;
    }



}
