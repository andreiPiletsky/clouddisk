package netcomteam.com.tgclouddisk.ui.adapters;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import netcomteam.com.tgclouddisk.R;
import netcomteam.com.tgclouddisk.pojo.CountryModel;
/**
 * Created by andreika on 03.10.17.
 */
public class CountryAdapter extends BaseAdapter {

    private List<CountryModel> countries;
    private LayoutInflater layoutInflater;
    private Context context;

    public class ViewHolder {

        TextView textAlphabetLetter;
        TextView textCountryName;
        TextView textCountryCode;
    }

    public CountryAdapter(Context context) {
        this.context = context;
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        preloadCountries();
    }

    @Override
    public int getCount() {
        return countries.size();
    }

    @Override
    public Object getItem(int position) {
        return countries.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.item_country, parent, false);
            viewHolder = new ViewHolder();
            viewHolder.textAlphabetLetter = (TextView) convertView.findViewById(R.id.letter);
            viewHolder.textCountryName = (TextView) convertView.findViewById(R.id.country_name);
            viewHolder.textCountryCode = (TextView) convertView.findViewById(R.id.country_code);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        CountryModel country = countries.get(position);
        boolean toShowFirstLetter;
        if (position == 0) {
            toShowFirstLetter = true;//always show it for the first element
        } else {
            char letterOfPrev = countries.get(position - 1).getName().charAt(0);
            char letterOfThis = country.getName().charAt(0);
            toShowFirstLetter = letterOfPrev != letterOfThis;
        }
        viewHolder.textAlphabetLetter.setText(toShowFirstLetter ? country.getName().substring(0, 1) : " ");
        viewHolder.textCountryName.setText(country.getName());
        viewHolder.textCountryCode.setText(country.getCode());
        return convertView;
    }

    public void preloadCountries() {
        countries = new ArrayList<>();
        String[] resource = context.getResources().getStringArray(R.array.country_codes);
        for (String row : resource) {
            String[] details = row.split(",");
            countries.add(new CountryModel(new Locale("", details[1]).getDisplayCountry(), details[0]));
        }
    }
}
