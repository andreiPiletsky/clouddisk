package netcomteam.com.tgclouddisk.ui.fragments;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.webkit.MimeTypeMap;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.github.angads25.filepicker.controller.DialogSelectionListener;
import com.github.angads25.filepicker.model.DialogConfigs;
import com.github.angads25.filepicker.model.DialogProperties;
import com.github.angads25.filepicker.view.FilePickerDialog;
import com.vincent.filepicker.Constant;
import com.vincent.filepicker.activity.AudioPickActivity;
import com.vincent.filepicker.activity.ImagePickActivity;
import com.vincent.filepicker.activity.NormalFilePickActivity;
import com.vincent.filepicker.activity.VideoPickActivity;
import com.vincent.filepicker.filter.entity.AudioFile;
import com.vincent.filepicker.filter.entity.ImageFile;
import com.vincent.filepicker.filter.entity.NormalFile;
import com.vincent.filepicker.filter.entity.VideoFile;
import com.wang.avi.AVLoadingIndicatorView;

import org.apache.commons.lang3.StringUtils;
import org.drinkless.td.libcore.telegram.Client;
import org.drinkless.td.libcore.telegram.TG;
import org.drinkless.td.libcore.telegram.TdApi;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import netcomteam.com.tgclouddisk.Const;
import netcomteam.com.tgclouddisk.R;
import netcomteam.com.tgclouddisk.handlers.ApiHelper;
import netcomteam.com.tgclouddisk.pojo.FileModel;
import netcomteam.com.tgclouddisk.pojo.FolderModel;
import netcomteam.com.tgclouddisk.ui.adapters.RecyclerItemClickListener;
import netcomteam.com.tgclouddisk.user.UserInformation;

import static android.app.Activity.RESULT_OK;
import static com.vincent.filepicker.activity.AudioPickActivity.IS_NEED_RECORDER;
import static com.vincent.filepicker.activity.ImagePickActivity.IS_NEED_CAMERA;
/**
 * Created by andreika on 09.10.17.
 */
public class BaseFragment extends Fragment {

    public ArrayList<FileModel> data = new ArrayList<>();
    AsynThread asynThread;
    public boolean isSelectionFiles = false;
    public int last_messages;
    ImageButton imageButtonCancel;
    ArrayList<Integer> attachFiles = new ArrayList<>();
    RecyclerView mRecyclerView;
    private int pastVisiblesItems, visibleItemCount, totalItemCount;
    GridLayoutManager gridLayoutManager;
    public boolean loading = true;
    @BindView(R.id.avi)
    AVLoadingIndicatorView avi;
    Menu menu;
    MenuItem itemPopupMenu;
    MenuItem itemRefresh;
    MenuItem selectFiles;
    public String nameFragment;
    Dialog dialog;
    public boolean isGetFolders;
    public boolean isHasFolder;
    public int folder_message_id;
    public String[] listFolder;
    public int countFolder;
    public int levelOfDirectory;
    List<String> folderList = new ArrayList<>();
    public String pathFolder = "/";

    @OnClick(R.id.sendFileToCloud)
    void sendFileTocloud() {
        initPickerFiles(getActivity());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {
        final View view =
                inflater.inflate(R.layout.file_manager_content, container, false);
        setHasOptionsMenu(true);
        ButterKnife.bind(this, view);
        getUpdatesFromTelegram();
        imageButtonCancel = (ImageButton) getActivity().findViewById(R.id.imageButtonCancel);
        initGUI(view);
        initAdapter();
        loadData();
        adapterClickHandler();
        return view;
    }

    void initGUI(View view) {
        gridLayoutManager = new GridLayoutManager(getActivity(), 4);
        mRecyclerView = (RecyclerView) view.findViewById(R.id.list);
        mRecyclerView.setLayoutManager(gridLayoutManager);
    }

    public void loadData() {
        isGetFolders = false;
        last_messages = 0;
        data.clear();
        getFolder();
        getFiles();
    }

    void getFolder() {
        levelOfDirectory = 1;
        isHasFolder = false;
        TdApi.SearchChatMessages searchChatMessages = new TdApi.SearchChatMessages(new UserInformation().getId(),
                "FolderList", 0, Const.COUNT_LOAD_MESSAGES, new TdApi.SearchMessagesFilterEmpty());
        TG.getClientInstance().send(searchChatMessages, new Client.ResultHandler() {
            @Override
            public void onResult(TdApi.TLObject object) {
                TdApi.Messages messages = (TdApi.Messages) object;
                TdApi.Message message = (TdApi.Message) messages.messages[0];
                folder_message_id = message.id;
                if (TdApi.MessageText.CONSTRUCTOR == ((TdApi.MessageText) message.content).getConstructor()) {
                    TdApi.MessageText messageText = (TdApi.MessageText) message.content;
                    listFolder = messageText.text.split(System.getProperty("line.separator"));
                    for (int i = 1; i < listFolder.length; i++) {
                        int startPos = StringUtils.ordinalIndexOf(listFolder[i], "/", levelOfDirectory);
                        int endPos = StringUtils.ordinalIndexOf(listFolder[i], "/", levelOfDirectory + 1);
                        if ((startPos > -1) && (endPos > -1)) {
                            folderList.add(listFolder[i].substring(0, endPos + 1));
                        }
                    }
                    new ApiHelper().deleteRepeatFolder(folderList);
                    for (int i = 0; i < folderList.size(); i++) {
                        int startPos = StringUtils.ordinalIndexOf(folderList.get(i), "/", levelOfDirectory);
                        int endPos = StringUtils.ordinalIndexOf(folderList.get(i), "/", levelOfDirectory + 1);
                        if ((startPos > -1) && (endPos > -1)) {
                            countFolder++;
                            FileModel fileModel = new FileModel();
                            FolderModel folderModel = new FolderModel();
                            folderModel.setPath(folderList.get(i));
                            folderModel.setFolderName(folderList.get(i).substring(startPos + 1, endPos));
                            folderModel.setMessage_id(message.id);
                            fileModel.setFolder(folderModel);
                            data.add(fileModel);
                            isGetFolders = true;
                            asynThread = new BaseFragment.AsynThread();
                            asynThread.execute();
                        }
                    }
                    isGetFolders = true;
                }
            }
        });
    }

    void getFiles() {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                while (!isGetFolders) {
                }
                TdApi.GetChatHistory getChatHistory = new TdApi.GetChatHistory(new UserInformation().getId(),
                        last_messages, 0, Const.COUNT_LOAD_MESSAGES);
                TG.getClientInstance().send(getChatHistory, new Client.ResultHandler() {
                    @Override
                    public void onResult(TdApi.TLObject object) {
                        TdApi.Messages messages = (TdApi.Messages) object;
                        for (int i = 0; i < messages.messages.length; i++) {
                            last_messages = messages.messages[i].id;
                            TdApi.Message message = (TdApi.Message) messages.messages[i];
                            if (message.content.getConstructor() == TdApi.MessagePhoto.CONSTRUCTOR) {
                                TdApi.MessagePhoto photo = (TdApi.MessagePhoto) message.content;
                                if (photo.caption.equals(pathFolder)) {
                                    FileModel fileModel = new ApiHelper().getImage(message, photo);
                                    data.add(fileModel);
                                    TdApi.DownloadFile downloadFile = new TdApi.DownloadFile((fileModel.getFile_id()));
                                    TG.getClientInstance().send(downloadFile, new Client.ResultHandler() {
                                        @Override
                                        public void onResult(TdApi.TLObject object) {
                                        }
                                    });
                                }
                            } else if (message.content.getConstructor() == TdApi.MessageAudio.CONSTRUCTOR) {
                                TdApi.MessageAudio audio = (TdApi.MessageAudio) message.content;
                                if (audio.caption.equals(pathFolder)) {
                                    FileModel fileModel = new ApiHelper().getAudio(message, audio);
                                    data.add(fileModel);
//                                    asynThread = new BaseFragment.AsynThread();
//                                    asynThread.execute();
                                }
                            } else if (message.content.getConstructor() == TdApi.MessageVideo.CONSTRUCTOR) {
                                TdApi.MessageVideo video = (TdApi.MessageVideo) message.content;
                                if (video.caption.equals(pathFolder)) {
                                    FileModel fileModel = new ApiHelper().getVideo(message, video);
                                    data.add(fileModel);
                                    Log.w("videoooo", String.valueOf(fileModel.getVideo().getPhoto_id()));
                                    TdApi.DownloadFile downloadFile = new TdApi.DownloadFile((fileModel.getVideo().getPhoto_id()));
                                    TG.getClientInstance().send(downloadFile, new Client.ResultHandler() {
                                        @Override
                                        public void onResult(TdApi.TLObject object) {
                                        }
                                    });
//                                    asynThread = new BaseFragment.AsynThread();
//                                    asynThread.execute();
                                }
                            } else if (message.content.getConstructor() == TdApi.MessageDocument.CONSTRUCTOR) {
                                TdApi.MessageDocument document = (TdApi.MessageDocument) message.content;
                                if (document.caption.equals(pathFolder)) {
                                    FileModel fileModel = new ApiHelper().getDocument(message, document);
                                    data.add(fileModel);
                                }
                            }
                        }
                        asynThread = new BaseFragment.AsynThread();
                        asynThread.execute();
                    }
                });
                loading = true;
                return null;
            }
        }.execute();
    }

    void initPickerFiles(Activity context) {
        dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_dialog);
        dialog.setTitle("Choose file!!!");
        LinearLayout sendImage = (LinearLayout) dialog.findViewById(R.id.sendImage);
        LinearLayout sendAudio = (LinearLayout) dialog.findViewById(R.id.sendMusic);
        LinearLayout sendVideo = (LinearLayout) dialog.findViewById(R.id.sendVideo);
        LinearLayout sendDocument = (LinearLayout) dialog.findViewById(R.id.sendDocument);
        sendImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pickImage();
            }
        });
//
        sendAudio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pickAudio();
            }
        });
//
        sendVideo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pickVideo();
            }
        });
        sendDocument.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pickDocument();
            }
        });
        dialog.show();
        LinearLayout sendFile = (LinearLayout) dialog.findViewById(R.id.sendFile);
        LinearLayout newFolder = (LinearLayout) dialog.findViewById(R.id.newFolder);
        sendFile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pickFile();
            }
        });
        newFolder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addFolder();
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        itemPopupMenu = menu.findItem(R.id.more_menu);
        itemRefresh = menu.findItem(R.id.refresh);
        selectFiles = menu.findItem(R.id.attach_files);
        final AppCompatActivity activity = (AppCompatActivity) getActivity();
        switch (item.getItemId()) {
            case R.id.attach_files: {
                activity.getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_side_bar);
                activity.getSupportActionBar().setDisplayHomeAsUpEnabled(false);
                activity.getSupportActionBar().setHomeButtonEnabled(false);
                activity.getSupportActionBar().setTitle("");
                isSelectionFiles = true;
                imageButtonCancel.setVisibility(View.VISIBLE);
                itemRefresh.setVisible(false);
                selectFiles.setVisible(false);
                itemPopupMenu.setVisible(true);
                itemPopupMenu.setEnabled(true);
                break;
            }
            case R.id.refresh:
                loadData();
                initAdapter();
                break;
            case R.id.item_move:
                Log.w("item", "move");
                break;
            case R.id.item_copy:
                Log.w("item", "copy");
                break;
            case R.id.item_download:
                downLoadFiles();
                initAppBar();
                imageButtonCancel.setVisibility(View.GONE);
                isSelectionFiles = false;
                itemRefresh.setVisible(true);
                selectFiles.setVisible(true);
                itemPopupMenu.setVisible(false);
                initAdapter();
                break;
            case R.id.item_delete:
                deleteFiles();
                initAppBar();
                isSelectionFiles = false;
                imageButtonCancel.setVisibility(View.GONE);
                selectFiles.setVisible(true);
                initAdapter();
                break;
        }
        imageButtonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isSelectionFiles = false;
                initAdapter();
                imageButtonCancel.setVisibility(View.GONE);
                itemRefresh.setVisible(true);
                selectFiles.setVisible(true);
                itemPopupMenu.setVisible(false);
                itemPopupMenu.setEnabled(false);
                initAppBar();
            }
        });
        return super.onOptionsItemSelected(item);
    }

    void initAppBar() {
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        this.menu = menu;
        inflater.inflate(R.menu.menu_toolbar, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    void adapterClickHandler() {
        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (dy > 0) {
                    visibleItemCount = gridLayoutManager.getChildCount();
                    totalItemCount = gridLayoutManager.getItemCount();
                    pastVisiblesItems = gridLayoutManager.findFirstVisibleItemPosition();
                    if (loading) {
                        if ((visibleItemCount + pastVisiblesItems) >= totalItemCount) {
                            loading = false;
                            getFiles();
                        }
                    }
                }
            }
        });
        mRecyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(getActivity(), new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        if (isSelectionFiles) {
                            final ImageView tickImage = (ImageView) view.findViewById(R.id.tick_image);
                            final ImageView mImg = (ImageView) view.findViewById(R.id.image_view);
                            if (!tickImage.isShown()) {
                                attachFiles.add(position);
                                Log.w("attachFiles", String.valueOf(attachFiles.get(0)));
                                tickImage.setVisibility(View.VISIBLE);
                            } else {
                                for (int i = 0; i < attachFiles.size(); i++) {
                                    if (attachFiles.get(i) == position) {
                                        attachFiles.remove(i);
                                    }
                                }
                                tickImage.setVisibility(View.INVISIBLE);
                            }
                        } else {
                            if (data.get(position).getFolder() != null) {
                                fragmentTransaction(data.get(position).getFolder().getPath(), data.get(position).getFolder().getFolderName());
                            }
                        }
                    }
                })
        );
    }

    void fragmentTransaction(String path, String name) {
        levelOfDirectory++;
        FolderFragment fragment = new FolderFragment();
        Bundle arguments = new Bundle();
        arguments.putString("pathFolder", path);
        arguments.putString("folderName", name);
        arguments.putInt("levelFolder", levelOfDirectory);
        fragment.setArguments(arguments);
        FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.main_container, fragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    void downLoadFiles() {
        ApiHelper fileHelper = new ApiHelper();
        fileHelper.downLoadFile(attachFiles, data);
        attachFiles.clear();
    }

    public void deleteFiles() {
        ApiHelper fileHelper = new ApiHelper();
        fileHelper.deleteFile(attachFiles, data, listFolder);
        attachFiles.clear();
    }

    class AsynThread extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            updateAdapter();
            loading = true;
        }
    }

    void initAdapter() {
    }

    void updateAdapter() {
    }

    void getUpdatesFromTelegram() {
        TG.setUpdatesHandler(new Client.ResultHandler() {
            @Override
            public void onResult(TdApi.TLObject object) {
//
                if (object.getConstructor() == TdApi.UpdateFileProgress.CONSTRUCTOR) {
                    final TdApi.UpdateFileProgress updateFileProgress = (TdApi.UpdateFileProgress) object;
                    for (int i = 0; i < data.size(); i++) {
                        if (data.get(i).getFile_id() == updateFileProgress.fileId) {
                            data.get(i).setReady(updateFileProgress.ready);
                            data.get(i).setSize(updateFileProgress.size);
                            new AsyncTask<Void, Void, Void>() {
                                @Override
                                protected Void doInBackground(Void... voids) {
                                    return null;
                                }

                                @Override
                                protected void onPostExecute(Void aVoid) {
                                    super.onPostExecute(aVoid);
                                    updateAdapter();
                                }
                            }.execute();
                        }
                    }
                }
                if (object.getConstructor() == TdApi.UpdateFile.CONSTRUCTOR) {
                    Log.w("object", String.valueOf(object));
                    TdApi.UpdateFile updateFile = (TdApi.UpdateFile) object;
                    for (int i = 0; i < data.size(); i++) {
                        if (data.get(i).getFile_id() == updateFile.file.id) {
                            data.get(i).setPath(updateFile.file.path);
                        }
                        if ((data.get(i).getVideo() != null) && (data.get(i).getVideo().getPhoto_id() == updateFile.file.id)) {
                            data.get(i).getVideo().setPhotoPath(updateFile.file.path);
                            Log.w("getvideo", updateFile.file.path);
                        }
//
                    }
                    asynThread = new BaseFragment.AsynThread();
                    asynThread.execute();
                }
            }
        });
    }

    private void pickImage() {
        dialog.dismiss();
        Intent intent = new Intent(getActivity(), ImagePickActivity.class);
        intent.putExtra(IS_NEED_CAMERA, true);
        intent.putExtra(Constant.MAX_NUMBER, 9);
        startActivityForResult(intent, Constant.REQUEST_CODE_PICK_IMAGE);
    }

    private void pickAudio() {
        dialog.dismiss();
        Intent intent = new Intent(getActivity(), AudioPickActivity.class);
        intent.putExtra(IS_NEED_RECORDER, true);
        intent.putExtra(Constant.MAX_NUMBER, 9);
        startActivityForResult(intent, Constant.REQUEST_CODE_PICK_AUDIO);
    }

    private void pickVideo() {
        dialog.dismiss();
        Intent intent = new Intent(getActivity(), VideoPickActivity.class);
        intent.putExtra(IS_NEED_CAMERA, true);
        intent.putExtra(Constant.MAX_NUMBER, 9);
        startActivityForResult(intent, Constant.REQUEST_CODE_PICK_VIDEO);
    }

    private void pickDocument() {
        dialog.dismiss();
        Intent intent = new Intent(getActivity(), NormalFilePickActivity.class);
        intent.putExtra(Constant.MAX_NUMBER, 9);
        intent.putExtra(NormalFilePickActivity.SUFFIX, new String[]{"xlsx", "xls", "doc", "docx", "ppt", "pptx", "pdf"});
        startActivityForResult(intent, Constant.REQUEST_CODE_PICK_FILE);
    }

    private void pickFile() {
        dialog.dismiss();
        DialogProperties properties = new DialogProperties();
        properties.selection_mode = DialogConfigs.MULTI_MODE;
        properties.selection_type = DialogConfigs.FILE_SELECT;
        properties.root = new File(DialogConfigs.DEFAULT_DIR);
        properties.error_dir = new File(DialogConfigs.DEFAULT_DIR);
        properties.offset = new File(DialogConfigs.DEFAULT_DIR);
        properties.extensions = null;
        FilePickerDialog dialog = new FilePickerDialog(getActivity(), properties);
        dialog.setTitle("Select a File");
        dialog.setDialogSelectionListener(new DialogSelectionListener() {
            @Override
            public void onSelectedFilePaths(String[] files) {
                for (int i = 0; i < files.length; i++) {
//                            files[i] = files[i].replace(" ", "_");
                    String extension = MimeTypeMap.getFileExtensionFromUrl(files[i]);
                    if (extension != null) {
                        String type = null;
                        try {
                            MimeTypeMap mime = MimeTypeMap.getSingleton();
                            type = mime.getMimeTypeFromExtension(extension);
                        } catch (Exception e) {
                        }
                        if (type != null) {
                            if (type.contains("image")) {
                                upLoadPhoto(files[i], pathFolder);
                            } else if (type.contains("video")) {
                                upLoadVideo(files[i], 0, 0, 0, pathFolder);
                            } else if (type.contains("audio")) {
                                upLoadAudio(files[i], 0, "", "", pathFolder);
                            }
                        } else {
                            upLoadDocument(files[i], pathFolder);
                        }
                    }
                }
            }
        });
        dialog.show();
    }

    private void addFolder() {
        dialog.dismiss();
        final Dialog inputFolder = new Dialog(getActivity());
        inputFolder.requestWindowFeature(Window.FEATURE_NO_TITLE);
        inputFolder.setContentView(R.layout.input_folder_dialog);
        inputFolder.setTitle("Choose file!!!");
        inputFolder.show();
        Button cancel = (Button) inputFolder.findViewById(R.id.cancel_dialog);
        Button submit = (Button) inputFolder.findViewById(R.id.submit_dialog);
        final EditText editText = (EditText) inputFolder.findViewById(R.id.input_folder_name);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                inputFolder.dismiss();
            }
        });
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new ApiHelper().createFolder(folder_message_id, listFolder, pathFolder + editText.getText().toString() + "/");
                inputFolder.dismiss();
                loadData();
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intentData) {
        ApiHelper fileHelper = new ApiHelper();
        switch (requestCode) {
            case Constant.REQUEST_CODE_PICK_IMAGE:
                if (resultCode == RESULT_OK) {
                    ArrayList<ImageFile> list = intentData.getParcelableArrayListExtra(Constant.RESULT_PICK_IMAGE);
                    for (int i = 0; i < list.size(); i++) {
                        upLoadPhoto(list.get(i).getPath(), pathFolder);
                    }
                }
                break;
            case Constant.REQUEST_CODE_PICK_VIDEO:
                if (resultCode == RESULT_OK) {
                    ArrayList<VideoFile> list = intentData.getParcelableArrayListExtra(Constant.RESULT_PICK_VIDEO);
                    for (int i = 0; i < list.size(); i++) {
                        upLoadVideo(list.get(i).getPath(), (int) list.get(i).getDuration(), 0, 0, pathFolder);
                    }
                }
                break;
            case Constant.REQUEST_CODE_PICK_AUDIO:
                if (resultCode == RESULT_OK) {
                    ArrayList<AudioFile> list = intentData.getParcelableArrayListExtra(Constant.RESULT_PICK_AUDIO);
                    for (int i = 0; i < list.size(); i++) {
                        upLoadAudio(list.get(i).getPath(), 0, "", "", pathFolder);
                    }
                }
                break;
            case Constant.REQUEST_CODE_PICK_FILE:
                if (resultCode == RESULT_OK) {
                    ArrayList<NormalFile> list = intentData.getParcelableArrayListExtra(Constant.RESULT_PICK_FILE);
                    for (int i = 0; i < list.size(); i++) {
                        upLoadDocument(list.get(i).getPath(), pathFolder);
                    }
                }
                break;
        }
    }

    private void upLoadDocument(String filepath, String folder) {
        TdApi.InputFileLocal inputFileLocal = new TdApi.InputFileLocal(filepath);
        TdApi.InputMessageDocument inputMessageDocument = new TdApi.InputMessageDocument(inputFileLocal, null, folder);
        TdApi.SendMessage sendMessage = new TdApi.SendMessage(new UserInformation().getId(), 0, false, false, null, inputMessageDocument);
        TG.getClientInstance().send(sendMessage, new Client.ResultHandler() {
            @Override
            public void onResult(TdApi.TLObject object) {
                TdApi.Message message = (TdApi.Message) object;
                TdApi.MessageDocument document = (TdApi.MessageDocument) message.content;
                FileModel fileModel = new ApiHelper().getDocument(message, document);
                data.add(countFolder, fileModel);
                asynThread = new BaseFragment.AsynThread();
                asynThread.execute();
            }
        });
    }

    public void upLoadPhoto(String filepath, String folder) {
        TdApi.InputFileLocal inputFileLocal = new TdApi.InputFileLocal(filepath);
        TdApi.InputMessagePhoto inputMessagePhoto = new TdApi.InputMessagePhoto(inputFileLocal, folder);
        TdApi.SendMessage sendMessage = new TdApi.SendMessage(new UserInformation().getId(), 0, false, false, null, inputMessagePhoto);
        TG.getClientInstance().send(sendMessage, new Client.ResultHandler() {
            @Override
            public void onResult(TdApi.TLObject object) {
//                Log.w("object", String.valueOf(object));
                TdApi.Message message = (TdApi.Message) object;
                TdApi.MessagePhoto photo = (TdApi.MessagePhoto) message.content;
                FileModel fileModel = new ApiHelper().getImage(message, photo);
                data.add(countFolder, fileModel);
                asynThread = new BaseFragment.AsynThread();
                asynThread.execute();
            }
        });
    }

    public void upLoadVideo(String filepath, int duration, int width, int height, String folder) {
        TdApi.InputFileLocal inputFileLocal = new TdApi.InputFileLocal(filepath);
        TdApi.InputMessageVideo inputMessageVideo = new TdApi.InputMessageVideo(inputFileLocal, null, duration, width, height, folder);
        TdApi.SendMessage sendMessage = new TdApi.SendMessage(new UserInformation().getId(), 0, false, false, null, inputMessageVideo);
        TG.getClientInstance().send(sendMessage, new Client.ResultHandler() {
            @Override
            public void onResult(TdApi.TLObject object) {
                TdApi.Message message = (TdApi.Message) object;
                TdApi.MessageVideo video = (TdApi.MessageVideo) message.content;
                FileModel fileModel = new ApiHelper().getVideo(message, video);
                data.add(countFolder, fileModel);
                asynThread = new BaseFragment.AsynThread();
                asynThread.execute();
            }
        });
    }

    public void upLoadAudio(String filepath, int duration, String title, String performer, String folder) {
        TdApi.InputFileLocal inputFileLocal = new TdApi.InputFileLocal(filepath);
        TdApi.InputMessageAudio inputMessageAudio = new TdApi.InputMessageAudio(inputFileLocal, null, duration, title, performer, folder);
        TdApi.SendMessage sendMessage = new TdApi.SendMessage(new UserInformation().getId(), 0, false, false, null, inputMessageAudio);
        TG.getClientInstance().send(sendMessage, new Client.ResultHandler() {
            @Override
            public void onResult(TdApi.TLObject object) {
                TdApi.Message message = (TdApi.Message) object;
                TdApi.MessageAudio audio = (TdApi.MessageAudio) message.content;
                FileModel fileModel = new ApiHelper().getAudio(message, audio);
                data.add(countFolder, fileModel);
                asynThread = new BaseFragment.AsynThread();
                asynThread.execute();
            }
        });
    }
}