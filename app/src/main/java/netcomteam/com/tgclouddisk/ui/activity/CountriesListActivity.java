package netcomteam.com.tgclouddisk.ui.activity;//package netcomteam.com.newtgdisk.ui.activity;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import netcomteam.com.tgclouddisk.R;
import netcomteam.com.tgclouddisk.ui.adapters.CountryAdapter;
public class CountriesListActivity extends AppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @OnClick(R.id.toolbar)
    void cancelResult() {
        setResult(Activity.RESULT_CANCELED);
        finish();
    }

    @BindView(R.id.countries_list)
    ListView countriesList;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_countries);
        ButterKnife.bind(this);
        toolbar.setTitle(getResources().getString(R.string.label_country));
        toolbar.setTitleTextColor(Color.WHITE);
        CountryAdapter countryAdapter = new CountryAdapter(this);
        countriesList.setAdapter(countryAdapter);
        countriesList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                TextView name = (TextView) ((RelativeLayout) view).getChildAt(1);
                TextView code = (TextView) ((RelativeLayout) view).getChildAt(2);
                Intent intent = new Intent();
                intent.putExtra("name", name.getText().toString());
                intent.putExtra("code", code.getText().toString());
                setResult(Activity.RESULT_OK, intent);
                finish();
            }
        });
    }
}
