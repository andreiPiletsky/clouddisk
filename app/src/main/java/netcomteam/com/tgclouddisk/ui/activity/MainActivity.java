package netcomteam.com.tgclouddisk.ui.activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;

import com.wang.avi.AVLoadingIndicatorView;

import butterknife.BindView;
import butterknife.ButterKnife;
import netcomteam.com.tgclouddisk.R;
import netcomteam.com.tgclouddisk.ui.fragments.AudioFragment;
import netcomteam.com.tgclouddisk.ui.fragments.DocumentsFragment;
import netcomteam.com.tgclouddisk.ui.fragments.GalleryFragment;
import netcomteam.com.tgclouddisk.ui.fragments.MyDriveFragment;
import netcomteam.com.tgclouddisk.ui.fragments.VideoFragment;
import netcomteam.com.tgclouddisk.user.User;
public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.main_drawer)
    NavigationView mDrawer;
    @BindView(R.id.drawer_layout)
    DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle drawerToggle;
    private int mSelectedId;
    FragmentTransaction fragmentTransaction;
    @BindView(R.id.avi)
    AVLoadingIndicatorView avi;
    User user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        user = new User(MainActivity.this);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        setToolbar();
        initView();
        drawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, toolbar, R.string.drawer_open, R.string.drawer_close);
        mDrawerLayout.setDrawerListener(drawerToggle);
        drawerToggle.syncState();
        getInformationAboutUser();
    }

    private void setToolbar() {
        if (toolbar != null) {
            setSupportActionBar(toolbar);
        }
    }

    private void initView() {
        mDrawer.setNavigationItemSelectedListener(this);
        getSupportFragmentManager().addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {
            @Override
            public void onBackStackChanged() {
                if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
                    getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back);
                    getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                    toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            onBackPressed();
                        }
                    });
                } else {
                    getSupportActionBar().setDisplayHomeAsUpEnabled(false);
                    drawerToggle.syncState();
                    toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mDrawerLayout.openDrawer(GravityCompat.START);
                        }
                    });
                }
            }
        });
    }

    private void itemSelection(int mSelectedId) {
        switch (mSelectedId) {
            case R.id.my_drive:
                getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                mDrawerLayout.closeDrawer(GravityCompat.START);
                fragmentTransaction = getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.main_container, new MyDriveFragment());
                fragmentTransaction.commit();
                getSupportActionBar().setTitle(R.string.my_drive);
                break;
            case R.id.gallery:
                getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                mDrawerLayout.closeDrawer(GravityCompat.START);
                fragmentTransaction = getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.main_container, new GalleryFragment());
                fragmentTransaction.commit();
                getSupportActionBar().setTitle(R.string.gallery);
                break;
            case R.id.music:
                getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                mDrawerLayout.closeDrawer(GravityCompat.START);
                fragmentTransaction = getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.main_container, new AudioFragment());
                fragmentTransaction.commit();
                getSupportActionBar().setTitle(R.string.music);
                break;
            case R.id.video:
                getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                mDrawerLayout.closeDrawer(GravityCompat.START);
                fragmentTransaction = getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.main_container, new VideoFragment());
                fragmentTransaction.commit();
                getSupportActionBar().setTitle(R.string.video);
                break;
            case R.id.documents:
                getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                mDrawerLayout.closeDrawer(GravityCompat.START);
                fragmentTransaction = getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.main_container, new DocumentsFragment());
                fragmentTransaction.commit();
                getSupportActionBar().setTitle(R.string.documents);
                break;
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        drawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        item.setChecked(true);
        mSelectedId = item.getItemId();
        itemSelection(mSelectedId);
        return true;
    }

    @Override
    public void onSaveInstanceState(Bundle outState, PersistableBundle outPersistentState) {
        super.onSaveInstanceState(outState, outPersistentState);
        outState.putInt("SELECTED_ID", mSelectedId);
    }

    void getInformationAboutUser() {
        UserInfo userInfo = new UserInfo();
        userInfo.execute();
    }

    public void transactionToMydriveFragment() {
        getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        mDrawerLayout.closeDrawer(GravityCompat.START);
        fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.main_container, new MyDriveFragment());
        fragmentTransaction.commit();
        getSupportActionBar().setTitle(R.string.my_drive);
    }

    private class UserInfo extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... voids) {
            user.setGetInformationAboutUser(false);
            user.getInformationAboutUser();
            while (!user.isGetInformationAboutUser()) {
            }
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            avi.hide();
            user.log_out(mDrawer);
            transactionToMydriveFragment();
        }
    }
}