package netcomteam.com.tgclouddisk.ui.activity;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import com.yanzhenjie.permission.AndPermission;
import com.yanzhenjie.permission.Permission;
import com.yanzhenjie.permission.PermissionListener;

import org.drinkless.td.libcore.telegram.Client;
import org.drinkless.td.libcore.telegram.TG;
import org.drinkless.td.libcore.telegram.TdApi;

import java.util.List;

import netcomteam.com.tgclouddisk.R;
public class SplashScreenActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash);
        checkPermission();
    }

    private void checkPermission() {
        AndPermission.with(getApplicationContext())
                .requestCode(200)
                .permission(Permission.STORAGE)
                .callback(listener)
                .start();
    }

    private PermissionListener listener = new PermissionListener() {
        @Override
        public void onSucceed(int requestCode, List<String> grantedPermissions) {
            if (requestCode == 200) {
                chooseActivity();
            }
        }

        @Override
        public void onFailed(int requestCode, List<String> deniedPermissions) {
            // Failure.
            if (requestCode == 200) {
                Toast.makeText(getApplicationContext(), "Add permission for application", Toast.LENGTH_SHORT).show();
            }
        }
    };

    private void chooseActivity() {
        TdApi.GetAuthState authStateRequest = new TdApi.GetAuthState();
        TG.getClientInstance().send(authStateRequest, new Client.ResultHandler() {
            @Override
            public void onResult(TdApi.TLObject object) {
                if (object instanceof TdApi.AuthStateOk) {
                    startActivity(new Intent(getApplicationContext(), MainActivity.class));
                    finish();
                } else if (object instanceof TdApi.AuthStateWaitCode) {
                    startActivity(new Intent(getApplicationContext(), ActivationActivity.class));
                    finish();
                } else if (object instanceof TdApi.AuthStateWaitPhoneNumber) {
                    startActivity(new Intent(getApplicationContext(), AutorizationActivity.class));
                    finish();
                } else if (object instanceof TdApi.Error) {
                    TG.getClientInstance().send(new TdApi.ResetAuth(), new Client.ResultHandler() {
                        @Override
                        public void onResult(TdApi.TLObject object) {
                        }
                    });
                } else {
                    startActivity(new Intent(getApplicationContext(), AutorizationActivity.class));
                    finish();
                }
            }
        });
    }
}
