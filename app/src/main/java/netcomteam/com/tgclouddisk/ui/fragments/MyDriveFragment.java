package netcomteam.com.tgclouddisk.ui.fragments;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import netcomteam.com.tgclouddisk.R;
import netcomteam.com.tgclouddisk.ui.adapters.MyDriveAdapter;
public class MyDriveFragment extends BaseFragment {

    MyDriveAdapter mAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        nameFragment = getResources().getString(R.string.my_drive);
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    void initAdapter() {
        mAdapter = new MyDriveAdapter(getActivity(), data);
        mRecyclerView.setAdapter(mAdapter);
    }

    @Override
    void updateAdapter() {
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void onResume() {
        super.onResume();
        initAppBar();
    }

    void initAppBar() {
        final AppCompatActivity activity = (AppCompatActivity) getActivity();
        activity.getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_side_bar);
        activity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        activity.getSupportActionBar().setHomeButtonEnabled(true);
        activity.getSupportActionBar().setTitle(R.string.my_drive);
    }
}