package netcomteam.com.tgclouddisk.ui.fragments;
import android.content.Intent;
import android.net.Uri;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.Button;
import android.widget.ImageView;

import org.drinkless.td.libcore.telegram.Client;
import org.drinkless.td.libcore.telegram.TG;
import org.drinkless.td.libcore.telegram.TdApi;

import java.io.File;

import netcomteam.com.tgclouddisk.Const;
import netcomteam.com.tgclouddisk.R;
import netcomteam.com.tgclouddisk.handlers.ApiHelper;
import netcomteam.com.tgclouddisk.pojo.ChatModel;
import netcomteam.com.tgclouddisk.pojo.FileModel;
import netcomteam.com.tgclouddisk.ui.adapters.MyDriveAdapter;
import netcomteam.com.tgclouddisk.ui.adapters.RecyclerItemClickListener;
/**
 * A simple {@link Fragment} subclass.
 */
public class DocumentsFragment extends BaseFragment {

    MyDriveAdapter mAdapter;

    @Override
    void initAdapter() {
        mAdapter = new MyDriveAdapter(getActivity(), data);
        mRecyclerView.setAdapter(mAdapter);
    }

    @Override
    void initGUI(View view) {
        gridLayoutManager = new GridLayoutManager(getActivity(), 4);
        mRecyclerView = (RecyclerView) view.findViewById(R.id.list);
        mRecyclerView.setLayoutManager(gridLayoutManager);
        Button button = (Button) view.findViewById(R.id.sendFileToCloud);
        button.setVisibility(View.INVISIBLE);
    }

    @Override
    public void loadData() {
        last_messages = 0;
        data.clear();
        getFiles();
    }

    @Override
    void getFiles() {
        TdApi.SearchChatMessages searchChatMessages = new TdApi.SearchChatMessages(new ChatModel().getId(),
                "", last_messages, Const.COUNT_LOAD_MESSAGES, new TdApi.SearchMessagesFilterDocument());
        TG.getClientInstance().send(searchChatMessages, new Client.ResultHandler() {
            @Override
            public void onResult(TdApi.TLObject object) {
                Log.w("messagessssssssssss", String.valueOf(object));
                TdApi.Messages messages = (TdApi.Messages) object;
                for (int i = 0; i < messages.messages.length; i++) {
                    last_messages = messages.messages[i].id;
                    TdApi.Message message = (TdApi.Message) messages.messages[i];
                    if (message.content.getConstructor() == TdApi.MessageDocument.CONSTRUCTOR) {
                        TdApi.MessageDocument document = (TdApi.MessageDocument) message.content;
                        FileModel fileModel = new ApiHelper().getDocument(message, document);
                        data.add(fileModel);
                    }
                }
                asynThread = new BaseFragment.AsynThread();
                asynThread.execute();
            }
        });
        loading = true;
    }

    @Override
    void adapterClickHandler() {
        mRecyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(getActivity(), new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        if (isSelectionFiles) {
                            final ImageView tickImage = (ImageView) view.findViewById(R.id.tick_image);
                            final ImageView mImg = (ImageView) view.findViewById(R.id.image_view);
                            if (!tickImage.isShown()) {
                                attachFiles.add(position);
                                tickImage.setVisibility(View.VISIBLE);
                            } else {
                                for (int i = 0; i < attachFiles.size(); i++) {
                                    if (attachFiles.get(i) == position) {
                                        attachFiles.remove(i);
                                    }
                                }
                                tickImage.setVisibility(View.INVISIBLE);
                            }
                        } else {
                            if (!data.get(position).getPath().equals("")) {
                                try {
                                    String path = "File Path";
                                    Intent intent = new Intent();
                                    intent.setAction(android.content.Intent.ACTION_VIEW);
                                    File file = new File(path);
                                    MimeTypeMap mime = MimeTypeMap.getSingleton();
                                    String ext = file.getName().substring(file.getName().indexOf(".") + 1);
                                    String type = mime.getMimeTypeFromExtension(ext);
                                    intent.setDataAndType(Uri.fromFile(file), type);
                                    startActivity(intent);
                                } catch (Exception e) {
// TODO: handle exception
                                    String data = e.getMessage();
                                }
                            }
                        }
                    }
                })
        );
    }

    @Override
    void updateAdapter() {
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void onResume() {
        super.onResume();
        initAppBar();
    }

    void initAppBar() {
        final AppCompatActivity activity = (AppCompatActivity) getActivity();
        activity.getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_side_bar);
        activity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        activity.getSupportActionBar().setHomeButtonEnabled(true);
        activity.getSupportActionBar().setTitle(R.string.documents);
    }
}
