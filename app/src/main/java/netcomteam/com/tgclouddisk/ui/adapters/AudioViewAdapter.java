package netcomteam.com.tgclouddisk.ui.adapters;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import netcomteam.com.tgclouddisk.R;
import netcomteam.com.tgclouddisk.pojo.AudioModel;
/**
 * Created by JUNED on 6/16/2016.
 */
public class AudioViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private LayoutInflater inflater;
    Context context;
    List<AudioModel> data = new ArrayList<>();

    public AudioViewAdapter(Context context, List<AudioModel> data) {
        this.context = context;
        this.data = data;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder;
        View v;
        v = LayoutInflater.from(parent.getContext()).inflate(
                R.layout.item_file, parent, false);
        viewHolder = new AudioViewAdapter.MyItemHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        Picasso picasso = Picasso.with(context);
        picasso
                .load(R.drawable.music)
                .resize(200, 200)
                .centerInside()
                .into(((AudioViewAdapter.MyItemHolder) holder).mImg);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class MyItemHolder extends RecyclerView.ViewHolder {

        ImageView mImg;

        public MyItemHolder(View itemView) {
            super(itemView);
//            mImg = (ImageView) itemView.findViewById(R.id.i);
        }
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }
}
