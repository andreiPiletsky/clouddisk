package netcomteam.com.tgclouddisk.handlers;
import android.util.Log;

import org.drinkless.td.libcore.telegram.Client;
import org.drinkless.td.libcore.telegram.TG;
import org.drinkless.td.libcore.telegram.TdApi;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.List;

import netcomteam.com.tgclouddisk.Const;
import netcomteam.com.tgclouddisk.pojo.AudioModel;
import netcomteam.com.tgclouddisk.pojo.ChatModel;
import netcomteam.com.tgclouddisk.pojo.DocumentModel;
import netcomteam.com.tgclouddisk.pojo.FileModel;
import netcomteam.com.tgclouddisk.pojo.ImageModel;
import netcomteam.com.tgclouddisk.pojo.UserModel;
import netcomteam.com.tgclouddisk.pojo.VideoModel;
import netcomteam.com.tgclouddisk.user.UserInformation;
/**
 * Created by andreika on 19.10.17.
 */
public class ApiHelper {

    ArrayList<String> folderName = new ArrayList<>();

    public FileModel getImage(TdApi.Message message, TdApi.MessagePhoto photo) {
        TdApi.File file = (TdApi.File) photo.photo.sizes[photo.photo.sizes.length - 1].photo;
        FileModel fileModel = new FileModel();
        fileModel.setMessage_id(message.id);
        ImageModel imageModel = new ImageModel();
        imageModel.setHeight(photo.photo.sizes[photo.photo.sizes.length - 1].height);
        imageModel.setWidth(photo.photo.sizes[photo.photo.sizes.length - 1].width);
        imageModel.setType(photo.photo.sizes[photo.photo.sizes.length - 1].type);
        imageModel.setPhoto_id(photo.photo.id);
        fileModel.setImage(imageModel);
        fileModel.setFile_id(file.id);
        fileModel.setPath(file.path);
        return fileModel;
    }

    public FileModel getAudio(TdApi.Message message, TdApi.MessageAudio audio) {
        TdApi.File file = (TdApi.File) audio.audio.audio;
        FileModel fileModel = new FileModel();
        fileModel.setMessage_id(message.id);
        AudioModel audioModel = new AudioModel();
        audioModel.setTitle(audio.audio.title);
        audioModel.setPerformer(audio.audio.performer);
        audioModel.setDuration(audio.audio.duration);
        audioModel.setFileName(audio.audio.fileName);
        audioModel.setMimeType(audio.audio.mimeType);
        fileModel.setFile_id(file.id);
        fileModel.setAudio(audioModel);
        fileModel.setPath(file.path);
        return fileModel;
    }

    public FileModel getVideo(TdApi.Message message, TdApi.MessageVideo video) {
        TdApi.File file = (TdApi.File) video.video.video;
        FileModel fileModel = new FileModel();
        fileModel.setMessage_id(message.id);
        VideoModel videoModel = new VideoModel();
        videoModel.setDuration(video.video.duration);
        videoModel.setWidth(video.video.width);
        videoModel.setHeight(video.video.height);
        videoModel.setFileName(String.valueOf(file.id));
        videoModel.setMimeType(video.video.mimeType);
        videoModel.setPhoto_id(video.video.thumb.photo.id);
        videoModel.setPhotoPath(video.video.thumb.photo.path);
        fileModel.setFile_id(file.id);
        fileModel.setPath(file.path);
        fileModel.setVideo(videoModel);
        return fileModel;
    }

    public FileModel getDocument(TdApi.Message message, TdApi.MessageDocument document) {
        TdApi.File file = (TdApi.File) document.document.document;
        FileModel fileModel = new FileModel();
        fileModel.setMessage_id(message.id);
        DocumentModel documentModel = new DocumentModel();
        documentModel.setFileName(document.document.fileName);
        documentModel.setMimeType(document.document.mimeType);
        fileModel.setFile_id(file.id);
        fileModel.setDocument(documentModel);
        fileModel.setPath(file.path);
        return fileModel;
    }

    public void CreateFolderMessage() {
        TdApi.SearchChatMessages searchChatMessages = new TdApi.SearchChatMessages(new ChatModel().getId(),
                "FolderList", 0, Const.COUNT_LOAD_MESSAGES, new TdApi.SearchMessagesFilterEmpty());
        TG.getClientInstance().send(searchChatMessages, new Client.ResultHandler() {
            @Override
            public void onResult(TdApi.TLObject object) {
//                Log.w("folder", String.valueOf(object));
                TdApi.Messages messages = (TdApi.Messages) object;
                if (messages.messages.length == 0) {
                    TdApi.InputMessageText inputMessageText = new TdApi.InputMessageText("FolderList", false, false, null, null);
                    TdApi.SendMessage sendMessage = new TdApi.SendMessage(new UserModel().getId(), 0, false, false, null, inputMessageText);
                    TG.getClientInstance().send(sendMessage, new Client.ResultHandler() {
                        @Override
                        public void onResult(TdApi.TLObject object) {
                        }
                    });
                }
            }
        });
    }

    public void createFolder(int message_id, String[] listFolder, String folderName) {
        String newMessageText = "";
        for (int i = 0; i < listFolder.length; i++) {
            newMessageText = newMessageText + listFolder[i] + System.getProperty("line.separator");
        }
        newMessageText = newMessageText + folderName;
        Log.w(String.valueOf(message_id), newMessageText);
        TdApi.InputMessageText inputMessageText = new TdApi.InputMessageText(newMessageText, false, false, null, null);
        TdApi.EditMessageText editMessageText = new TdApi.EditMessageText(new ChatModel().getId(), message_id, null, inputMessageText);
        TG.getClientInstance().send(editMessageText, new Client.ResultHandler() {
            @Override
            public void onResult(TdApi.TLObject object) {
            }
        });
    }

    public void deleteFolder(String path_folder_for_delete) {
//        Log.w("pathfolder", path_folder_for_delete);
        for (int i = 1; i < folderName.size(); i++) {
            if (folderName.get(i).length() >= path_folder_for_delete.length()) {
                if (folderName.get(i).substring(0, path_folder_for_delete.length()).equals(path_folder_for_delete)) {
                    folderName.remove(i);
                }
            }
        }
    }

    public void downLoadFile(ArrayList<Integer> attachFiles, ArrayList<FileModel> data) {
        final int[] files_id = new int[attachFiles.size()];
        for (int i = 0; i < attachFiles.size(); i++) {
            files_id[i] = data.get(attachFiles.get(i)).getFile_id();
            TdApi.DownloadFile downloadFile = new TdApi.DownloadFile((files_id[i]));
            TG.getClientInstance().send(downloadFile, new Client.ResultHandler() {
                @Override
                public void onResult(TdApi.TLObject object) {
//                    Log.w("objectttttUpdate", String.valueOf(object));
                }
            });
        }
    }

    public void deleteFile(ArrayList<Integer> attachFiles, ArrayList<FileModel> data, String[] folder) {
        int message_id = 0;
        folderName.clear();
        for (int i = 0; i < folder.length; i++) {
            folderName.add(folder[i]);
        }
        Collections.sort(attachFiles, Collections.reverseOrder());
        int[] messageIds = new int[attachFiles.size()];
        for (int i = 0; i < attachFiles.size(); i++) {
            if (data.get(attachFiles.get(i)).getFolder() != null) {
                message_id = data.get(attachFiles.get(i)).getFolder().getMessage_id();
                deleteFolder(data.get(attachFiles.get(i)).getFolder().getPath());
                data.remove((int) (attachFiles.get(i)));
            } else {
                messageIds[i] = data.get(attachFiles.get(i)).getMessage_id();
                data.remove((int) (attachFiles.get(i)));
            }
        }
        String newMessage = "FolderList" + System.getProperty("line.separator");
        for (int i = 1; i < folderName.size(); i++) {
            newMessage = newMessage + folderName.get(i) + System.getProperty("line.separator");
        }
        TdApi.InputMessageText inputMessageText = new TdApi.InputMessageText(newMessage, false, false, null, null);
        TdApi.EditMessageText editMessageText = new TdApi.EditMessageText(new ChatModel().getId(), message_id, null, inputMessageText);
        TG.getClientInstance().send(editMessageText, new Client.ResultHandler() {
            @Override
            public void onResult(TdApi.TLObject object) {
            }
        });
        TdApi.DeleteMessages deleteMessages = new TdApi.DeleteMessages(new ChatModel().getId(), messageIds);
        TG.getClientInstance().send(deleteMessages, new Client.ResultHandler() {
            @Override
            public void onResult(TdApi.TLObject object) {
            }
        });
    }

    public void deleteFile(int position, ArrayList<FileModel> data) {
        int messageId[] = new int[1];
        messageId[0] = data.get(position).getMessage_id();
        Log.w("test____id", String.valueOf(messageId[0])+"  "+ new UserInformation().getId());
        TdApi.DeleteMessages deleteMessages = new TdApi.DeleteMessages(new UserInformation().getId(), messageId);
        TG.getClientInstance().send(deleteMessages, new Client.ResultHandler() {
            @Override
            public void onResult(TdApi.TLObject object) {
                Log.w("test____object", String.valueOf(object));
            }
        });
    }

    public void deleteRepeatFolder(List<String> folderLIst) {
        LinkedHashSet<String> lhs = new LinkedHashSet<String>();
        lhs.addAll(folderLIst);
        folderLIst.clear();
        folderLIst.addAll(lhs);
    }
}
