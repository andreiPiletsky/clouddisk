package netcomteam.com.tgclouddisk.pojo;
/**
 * Created by andreika on 18.10.17.
 */
public class DocumentModel {

    private String fileName;
    private String mimeType;

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getMimeType() {
        return mimeType;
    }

    public void setMimeType(String mimeType) {
        this.mimeType = mimeType;
    }
}
