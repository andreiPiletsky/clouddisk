package netcomteam.com.tgclouddisk.pojo;
/**
 * Created by andreika on 19.10.17.
 */
public class FolderModel {

    private String folderName;
    private int message_id;
    private String path;

    public String getFolderName() {
        return folderName;
    }

    public void setFolderName(String folderName) {
        this.folderName = folderName;
    }

    public int getMessage_id() {
        return message_id;
    }

    public void setMessage_id(int message_id) {
        this.message_id = message_id;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }
}
