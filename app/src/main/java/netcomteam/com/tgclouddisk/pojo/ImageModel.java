package netcomteam.com.tgclouddisk.pojo;

/**
 * Created by Suleiman19 on 10/22/15.
 */
public class ImageModel {

    private String type;
    private int width;
    private int height;
    private long photo_id;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public long getPhoto_id() {
        return photo_id;
    }

    public void setPhoto_id(long photo_id) {
        this.photo_id = photo_id;
    }
}
