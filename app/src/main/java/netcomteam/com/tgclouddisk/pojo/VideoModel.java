package netcomteam.com.tgclouddisk.pojo;
/**
 * Created by andreika on 16.10.17.
 */
public class VideoModel {

    private String photoPath;
    private int duration;
    private int width;
    private int height;
    private String fileName;
    private String mimeType;
    private int photo_id;

    public String getPhotoPath() {
        return photoPath;
    }

    public void setPhotoPath(String photoPath) {
        this.photoPath = photoPath;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getMimeType() {
        return mimeType;
    }

    public void setMimeType(String mimeType) {
        int pos = mimeType.lastIndexOf("/");
        mimeType = mimeType.substring(pos + 1);
        this.mimeType = mimeType;
    }

    public int getPhoto_id() {
        return photo_id;
    }

    public void setPhoto_id(int photo_id) {
        this.photo_id = photo_id;
    }
}
